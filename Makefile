###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05d - AnimalFarm0  - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test a program
###
### @author Lyon Singleton <lyonws@hawaii.edu>
### @date 18_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = gcc

CFLAGS = -g -Wall -Wextra

TARGET = main

all: $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h 
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h
	$(CC) $(CLFAFS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h
	$(CC) -c updateCats.c

main.o: main.c addCats.c catDatabase.h deleteCats.c reportCats.c updateCats.c catDatabase.c
	$(CC) $(CFLAGS) -c main.c

main: main.o addCats.o  deleteCats.o reportCats.o updateCats.o catDatabase.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o deleteCats.o reportCats.o updateCats.o catDatabase.o

clean:
	rm -f $(TARGET) *.o


