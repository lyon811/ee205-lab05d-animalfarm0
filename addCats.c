/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//


#include "addCats.h"
#include <stdio.h>
#include <stdlib.h>
#include "catDatabase.h"
#include <string.h>

int addCat( const char* name , enum gender gender  , enum breed breed,  bool isfixed , float weight  ) {

  
  if (strlen(name) == 0) {
  printf("ERROR, name can not be empty!\n");
  return 0;
 }
 
 if (sizearray == MAX_CAT - 1) {
   printf("ERROR, database is full!\n");
   return 0;
 }
 if (strlen(name) >= MAX_CAT_NAME - 1) {
  printf("ERROR, name can not be more then 30 letters!\n");
  return 0;
 }
 if (weight == 0) {
 printf("ERROR, weight must be greater then 0!\n");
 return 0;
 }
 
  
  
 

  names[size] = name;
  genders[size] = gender; 
  breeds[size] = breed;
  isfixeds[size] = isfixed;
  weights[size] = weight; 
  
  
  
  size++;    
  
  
  return size - 1;

} ;

