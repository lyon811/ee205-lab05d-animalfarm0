/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//





#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "updateCats.h"
#include "catDatabase.h"
#define numArrElements(names)  (sizeof(names) / sizeof(*names))
 void updateCatName (int index, const char* newName) {
      for(int i = 0; i <= size - 1; i++) {

          if ( strcmp(names[i], newName) == 0  ) {
             printf("ERROR, name already used\n");
             return ;
            }
        
        if (strlen(newName) >= 30) {
            printf("ERROR, name can not be more then 30 letters!\n");
            return ;
        }
   }
  names[index] = newName;
  printf("Success, new name!\n");
  return ;
  
}

bool fixCat(int index) {
 isfixeds[index] = !isfixeds[index] ;
}

float updateCatWeight (int index , float newWeight) {
    if (newWeight <= 0) {
        printf("ERROR, weight cant be 0!\n");
        return 0;
      }
  weights[index] = newWeight;
  return printf("Success, new weight!\n");
}
