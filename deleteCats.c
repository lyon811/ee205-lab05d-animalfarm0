/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//





#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "deleteCats.h"
#include "catDatabase.h"

void deleteAllCats() {
  printf("Deleteing Cats\n");
  memset(names, 0, sizeof(names));
  memset(genders, 0, sizeof(genders));
  memset(breeds, 0, sizeof(breeds));
  memset(weights, 0, sizeof(weights));
  memset(isfixeds, 0, sizeof(isfixeds));
  printf("All Cats Deleted\n");
  for(int i = 0; i <= 6; i++) {
   printf("cat index = [%u] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", i , names[i], genders[i], breeds[i], isfixeds[i], weights[i]) ;
   }
}