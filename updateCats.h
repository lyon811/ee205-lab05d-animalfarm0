/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//







#pragma once


#include "catDatabase.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

extern void updateCatName(int index, const char* newName);
extern bool fixCat(int index);
extern float updateCatWeight( int index, float newWeight);