/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file CatDatabase.c
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//



#include <stdio.h>
#include <stdbool.h>
#include "catDatabase.h"

size_t size ;
const char* names[MAX_CAT] ;
enum gender genders[MAX_CAT];  
enum breed breeds[MAX_CAT];
bool isfixeds[MAX_CAT];
float weights[MAX_CAT];


size_t sizearray = sizeof(names) ;
size_t size = 0 ;
