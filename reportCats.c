/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//





#include <stdio.h>
#include <stdlib.h>
#include "reportCats.h"
#include "catDatabase.h"
#include <string.h>
#include "addCats.h"
#include "deleteCats.h"
#define numArrElements(names)  (sizeof(names) / sizeof(names[0]))
void printCat(int index) {
    if (index < 0 || index > MAX_CAT -1 ) {
      printf("ERROR, invalid index!\n");
      return;
    }
  printf("cat index = [%lu] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n", index , names[index], genders[index], breeds[index], isfixeds[index], weights[index]) ;
}
  

void printAllCats() {
  int count = numArrElements(names) - 1;
  for(int i = 0; i < count; i++) {
  
  if (names[i] == '\0') {
             return  ;
  }
    printf("cat index = [%lu] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", i , names[i], genders[i], breeds[i], isfixeds[i], weights[i]) ;
  }
}

int findCat( const char* name) {
  
  for(int i = 0; i <= size - 1 ; ++i) {
  
    if (strcmp(names[i], name) == 0 ) {
        printf("cat index = [%lu] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", i , names[i], genders[i], breeds[i], isfixeds[i], weights[i])  ; 
        return i;
            }
    
    }
    printf("ERROR, cat not in database!\n");
    return 0;
        
    
  
  
}