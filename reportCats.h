/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//





#pragma once



#include "catDatabase.h"
#include <stdio.h>
#include <stdlib.h>


extern void printCat(int index);

extern void printAllCats();

extern int findCat( const char* name);