/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0  - EE 205 - Spr 2022
///
/// @file CatDatabase.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 24_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//





#pragma once




#include <stdio.h>
#include <stdbool.h>

#define MAX_CAT 60
#define MAX_CAT_NAME 30

 enum gender{UNKNOWN_GENDER, MALE, FEMALE}; 
 enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

extern const char* names[MAX_CAT] ;
extern enum gender genders[MAX_CAT];  
extern enum breed breeds[MAX_CAT];
extern bool isfixeds[MAX_CAT];
extern float weights[MAX_CAT];

extern size_t size ;
extern size_t sizearray ;

